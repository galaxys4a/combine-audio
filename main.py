from pathlib import Path
import sys
import subprocess
import logging

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


def rename_files(root: Path) -> int:
    i = 1
    for file in root.iterdir():
        if not file.is_dir():
            # file.rename(Path(root, Path(f"{i}")))
            file.rename(Path(f"{i}"))
            i += 1
    renamed_count = i - 1
    return renamed_count

def get_command(files_count: int) -> str:
    cmd = "ffmpeg"
    for i in range(1, files_count + 1):
        cmd += f" -i {i}"
    cmd += f" -filter_complex amix=inputs={files_count}:duration=longest output.mp3"
    logger.debug("cmd = %s", cmd)
    return cmd

def combine_audio(cmd: str, root: Path) -> None:
    subprocess.run([cmd])

def main() -> None:
    path_str = sys.argv[1]
    path = Path(path_str)
    files_count = rename_files(path)
    cmd = get_command(files_count)
    combine_audio(cmd, path)

if __name__ == "__main__":
    main()
